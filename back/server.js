var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');
var cors = require('cors');

var mongoClient = require ('mongodb').MongoClient;
var url = "mongodb://servermongo:27017/local";//docker
//var url = "mongodb://localhost:27017/local";//local

var pg = require ('pg');
var urlUsuarios = "postgres://docker:docker@postgre:5432/bdseguridad";//docker
//var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";//local
var clientePostgre =new pg.Client(urlUsuarios);

var md5 = require ('md5');
app.use(cors());

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2/collections/movimientos";
var apiKey = "apiKey=Yd1AFuAHyG0Y4k1dqOYOMK_9nTCn0-5x";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);

app.listen(port);

console.log('todo list RESTful API: ' + port);

app.get('/cuentasMongo/:idCliente/cuentas', function(req, res) {
 mongoClient.connect(url, function(err, db){
   if(err){
     console.log(err);
   }
   else{
     console.log("Connected successfully to server");
     var col = db.collection('movimientos');
     var param = req.params.idCliente;
      
     col.find({"idCliente":param}).toArray(function(err, docs){
      
       res.send(docs[0].cuentas);
       
     });
    
     db.close();
   }
 });
});


app.post('/movimientosMongo',function(req, res){
  mongoClient.connect(url,function(err,db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('movimientos');
      var fechaMovimiento = new Date().toJSON().slice(0,10);
      var horaMovimiento = new Date().toJSON().slice(11,19);
      var idCliente = req.body.idCliente;
      var cuenta = req.body.idCuent;
      var movimiento = req.body.movimiento;
 
        col.update(
         { "cuentas.idCuenta": parseInt(cuenta) },
         { $push: { "cuentas.$.movimientos": {"idMovimiento": movimiento.idMovimiento, 
         "importe": movimiento.importe, "fechaMovimiento" : fechaMovimiento,
         "horaMovimiento": horaMovimiento ,"latitud": movimiento.latitud, "longitud": movimiento.longitud} } }
      );
 
 
      res.send("OK");
      db.close();
    }
  });
 });

app.get('/cuentasMongo/:idCliente', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('movimientos');
      var param = req.params.idCliente;
      col.find({"idCliente":param}).toArray(function(err, docs){
        console.log(req.params.idcliente);
        res.send(docs);
      });
      db.close();
    }
  });
});

app.get('/cuentasMongo/:idCliente/cuentas/:ncuenta/movimientos', function(req, res) {

  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('movimientos');
      var param = req.params.idCliente;
      // col.find({"idCliente": parseInt(param)}).sort({importe:-1}).toArray(function(err, docs){
        col.find({"idCliente":param}, {"sort" : [["fechaMovimiento", "asc"]]}).toArray(function (err, docs){
        res.send(docs[0].cuentas[req.params.ncuenta].movimientos);
      });
     
      db.close();
    }
  });
 });


app.get('/cuentasMongo/:idCliente/cuentas/:ncuenta/', function(req, res) {
  
    mongoClient.connect(url, function(err, db){
      if(err){
        console.log(err);
      }else{
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        var param = req.params.idCliente;
        col.find({"idCliente":param}).toArray(function(err, docs){
        res.send(docs[0].cuentas[req.params.ncuenta]);
        });
       
        db.close();
      }
    });
   });


app.get('/usuariosMongo', function (req,res)
{
  // var usuario = req.body.usuario;
  // var password = req.body.password;
  mongoClient.connect(url,function(err,db){
    if (err)
    {
      console.log(err);
    }
    else{
    console.log("Connected sucessfully to server");
    var col = db.collection('Usuarios');
    col.find({usuario:"sergio",password:"sergio"}).toArray(function(err,docs){
      res.send(docs);
    });
    db.close();
  }
  })
});


app.post('/clientes', function(req, res){
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('movimientos');

      //insertar por body
      col.insertOne(req.body,function(err, r){
        console.log(r.insertedCount + ' registros insertados body');
        res.send(r);
      });
      db.close();
    }
  });
  });

app.post('/loginP', function(req,res) {
  console.log(req.body);
  clientePostgre.connect();
  const query = clientePostgre.query ('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password = $2;', [req.body.usuario, req.body.password], (err,result) => {
  if (err){
  console.log(err);
  res.send(err);
  }
  else {
    if (result.rows[0].count>= 1)
    {
    res.send("Login correcto");
    }
    else{
    res.send("Login incorrecto");
   }
  }
});
});

app.post('/registroP', function(req,res) {

  console.log(req.body);
  clientePostgre.connect();
  const consultar = clientePostgre.query ('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password = $2;', [req.body.usuario, req.body.password], (err,result) => {
  if (err){
  console.log(err);
  res.send(err);
  }
  else {
    if (result.rows[0].count>= 1)
    {
    res.send("El usuario ya existe");
     }
    else{
    const insertar = clientePostgre.query ('INSERT INTO usuarios(login,password) values($1,$2)', [req.body.usuario,req.body.password]);
    res.send("Registro correcto");
    }
  }
});
});



app.post('/login', function(req, res) {
 //res.sendFile(path.join(__dirname, 'index.html'));
   console.log(req.body);
   var usuario = req.body.usuario;
   var password = req.body.password;
   var datos = {"usuario":this.usuario,"password":this.password};
   console.log(usuario);
   console.log(password);

   var request = new XMLHttpRequest();
   request.open("GET", "mongodb://servermongo:27017/usuariosMongo", false);

   request.setRequestHeader("Content-Type", "application/json");
   request.send(JSON.stringify(datos));
   console.log(request.responseText);


   if ((usuario == 'Oscar') && (password == 'Rey')) {
     res.send("Cliente valido");
   } else {
     res.send("Cliente invalido");
   }

});

